# Generated by Django 3.0.5 on 2020-05-12 11:49

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0012_remove_myuser_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='image',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=255), blank=True, default=list, size=10),
        ),
    ]
