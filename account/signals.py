from django.db.models.signals import post_save, pre_delete, post_delete, pre_save
from django.dispatch import receiver
from .models import MyUser, Setting
from post.models import Post, Comment
import os
@receiver(post_save, sender=MyUser)
def user_setting_create(sender, instance, created, **kwargs):
    if created:
        Setting.objects.create(user=instance)


@receiver(post_save, sender=MyUser)
def user_setting_update(sender, instance, created, **kwargs):
    if not created:
        instance.setting.save()


@receiver(post_save, sender=Comment)
def user_add_comment_count(sender, instance, created, **kwargs):
    if created:
        instance.post.user.comments_count += 1
        instance.post.comment_count += 1
        instance.post.user.save()
        instance.post.save()


@receiver(post_delete, sender=Comment)
def user_remove_comment_count(sender, instance, **kwargs):
    instance.post.user.comments_count -= 1
    instance.post.comment_count -= 1
    instance.post.user.save()
    instance.post.save()


@receiver(post_delete, sender=Post)
def user_remove_comment_count(sender, instance, **kwargs):
    for media in instance.medias:
        os.remove(media) if os.path.isfile(media) is True else print()

    for thumbnail in instance.thumbnail:
        os.remove(thumbnail) if os.path.isfile(thumbnail) is True else print()




