from django.contrib.auth.hashers import check_password
from account.api.lib.utils import lists_match
from account.models import MyUser
from api.excepts import ErrorHandle, ErrorTypes


def LoginController(req):
    try:
        user = MyUser.objects.get(username=req['username'])
        match = check_password(req['password'], user.password)
        if match:
            return user
        else:
            raise MyUser.DoesNotExist()
    except MyUser.DoesNotExist:
        return None


def RegisterController(req: dict):
    fields = ['username', 'password2', 'password']
    req_fields = req.keys()
    match = lists_match(fields, req_fields)
    if match is True:

        try:
            user = MyUser.objects.get(username=req['username'])
            ErrorHandle(ErrorTypes.UserExist(), False)

        except MyUser.DoesNotExist:
            if req['password'] != req['password2']:
                ErrorHandle(ErrorTypes.PasswordNotMatch(), False)
            user = MyUser.objects.create(
                username=req['username']
            )
            user.set_password(req['password'])
            user.save()
            return user

    else:
        ErrorHandle(ErrorTypes.FieldValidation(), False)


def GetUser(query: dict):
    try:
        user = MyUser.objects.get(**query)
        return user
    except MyUser.DoesNotExist:
        ErrorHandle(ErrorTypes.UserNotExist(), False)
