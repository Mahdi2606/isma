from account.models import Follow, MyUser
from account.api.controllers import userController
from api.excepts import ErrorTypes, ErrorHandle, FinalExcept


def CheckFollow(follower, following):
    try:
        follow = Follow.objects.get(following=following, follower=follower)
        return follow
    except Follow.DoesNotExist:
        return None

    except Exception as e:
        ErrorHandle(ErrorTypes.OtherError(), False, e)


def Add_follow(follower: MyUser, following):
    follow = None
    try:
        user = userController.GetUser({"username": following})
        check_follow = CheckFollow(follower, user)
        if check_follow is None:
            follow = Follow(
                follower=follower,
                following=user,
                status=1 if user.setting.is_lock_page is True else 2
            )
            follow.save()

            if user.setting.is_lock_page is not True:
                user.follower_count = user.follower_count + 1
                follower.following_count = follower.following_count + 1
                user.save()
                follower.save()
            return follow

        else:
            ErrorHandle(ErrorTypes.FollowExist(), False)

    except Exception as e:
        if isinstance(follow, Follow):
            follow.delete()
        FinalExcept(e)


def Accept_follow(follower, following):
    try:
        user = userController.GetUser({"username": follower})
        follow = Follow.objects.get(following=following, follower=user)
        if follow.status is 2:
            ErrorHandle(ErrorTypes.FollowAccept(), False)

        follow.status = 2
        follow.save()
        return follow
    except Exception as e:
        FinalExcept(e)


def Delete_follow(follower, following):
    try:
        user = userController.GetUser({"username": follower})
        try:
            follow = Follow.objects.get(following=following, follower=user)
            follow.delete()
            return follow
        except Follow.DoesNotExist:
            ErrorHandle(ErrorTypes.FollowNotExist(), False)
    except Exception as e:
        FinalExcept(e)
