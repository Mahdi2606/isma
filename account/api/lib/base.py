#
# class base(type):
#     def __new__(self,name,bases,atts):
#         print(self,name,bases,atts)
#         return super().__new__(self , name , bases , atts)
#
#

class Serializer:

    fields = None
    relations = None

    @classmethod
    def ModelSerialize(self,model):
        if self.fields is not None and model is not None:
            data = {}
            for field in self.fields:
                data[field] = getattr(model, field)
            if self.relations:
                for item in self.relations:
                    data.update({item: {}})
                    relObj = getattr(model, item)
                    for key in self.relations[item]:
                        value = getattr(relObj, key)
                        data[item].update({key: value})
            return data
        else:
            raise TypeError('مقاداری وردی اشتباه میباشد')

    # def Serialize(self, obj, fields: list, rel: dict):

# def serialize(model, fields: list, rel=None):

