
def reqToDict(obj):
    req = dict(obj)
    data = {}
    for i in req:
        data.update({i: req[i][0]})

    return data


def getDicKeyToList(dict: dict):
    list = []
    for key in dict.keys():
        list.append(key)

    return list


def lists_match(List1, List2):
    match = True
    if len(List1) == len(List2):
        for i in List2:
            if i in List1:
                pass
            else:
                match = False
    else:
        match = False

    return match
