from account.api.config import TOKEN_SECRET_KEY
from jose import jwt


def token_generate(dic: dict):
    # try:
    token = jwt.encode(
        dic,
        TOKEN_SECRET_KEY,
    )
    return token
    # except:



def token_read(token : str):
    try:
        data = jwt.decode(
            token,
            TOKEN_SECRET_KEY,
        )
        return data
    except:
        return None




