from django.core import serializers
from django.http import QueryDict
from account.models import MyUser
from rest_framework import serializers
from account.models import MyUser
from .lib.base import Serializer
from account.api.controllers import followController
from post.api.controllers import postController
# class UserLoginSerializer(Serializer):
#     # model = MyUser
#     fields = ['username','likes','views']


class RegisterSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type'}, write_only=True)

    class Meta:
        model = MyUser
        fields = ['username', 'password', 'password2']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def save(self):
        user = MyUser(
            username=self.validated_data['username'],
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']
        if password != password2:
            raise serializers.ValidationError({'password': 'passwords must be match'})
        user.set_password(password)
        user.save()
        return user


class AuthUserSerializer(serializers.ModelSerializer):
    setting = serializers.SerializerMethodField('get_setting_from_user')
    fullname = serializers.SerializerMethodField('get_full_name')
    posts = serializers.SerializerMethodField('get_user_posts')

    class Meta:
        model = MyUser
        fields = ['username', 'uid', 'phone', 'email', 'fullname', 'date_of_birth', 'likes', 'views', 'follower_count',
                  'following_count', 'comments_count', 'setting', 'posts']

    def get_full_name(self, user):
        short_name = user.short_name if user.long_name is not None else ''
        long_name = user.long_name if user.long_name is not None else ''
        return f'{short_name} {long_name}'

    def get_setting_from_user(self, user):
        setting = user.setting
        return {
            'page_lock': setting.is_lock_page,
            'comment_lock': setting.is_lock_comment
        }

    def get_user_posts(self, user):
        all_posts = user.posts.all()
        posts = []

        for post in all_posts:
            posts.append(post.uid)

        # for post in all_posts:
        #     posts.append({
        #         "id": post.uid,
        #         "title": post.title,
        #         "body": post.body,
        #         "like": post.like,
        #         "view": post.view,
        #         "comment_count": post.comment_count,
        #         "hash_tags": post.hash_tags,
        #         "mentions": post.mentions,
        #     })

        return posts


class UserSerializer(serializers.ModelSerializer):
    fullname = serializers.SerializerMethodField('get_full_name')
    posts = serializers.SerializerMethodField('get_posts')
    follow = serializers.SerializerMethodField('get_follow_status')

    class Meta:
        model = MyUser
        fields = ['uid', 'username', 'fullname', 'likes', 'posts', 'follow']


    def get_full_name(self, user):
        short_name = user.short_name if user.long_name is not None else ''
        long_name = user.long_name if user.long_name is not None else ''
        return f'{short_name} {long_name}'

    def get_follow_status(self, user):
        auth_user = self.context.get("auth_user")
        follow = followController.CheckFollow(auth_user, user)
        if follow is None:
            return 0
        return follow.status

    def get_posts(self, user):
        auth_user = self.context.get("auth_user")
        access = postController.AccessToPosts(auth_user, user)
        if access in [0, 1]:
            return 'This page is private'

        all_posts = user.posts.all()
        posts = []
        for post in all_posts:
            posts.append(post.uid)
        return posts

