import uuid
from django.http import JsonResponse
from account.models import MyUser
from .lib.token_manager import token_read
from api import config
from account.api.controllers import userController
from post.api.controllers import postController
from api.excepts import FinalExcept

class AuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        view = view_func.__name__
        req_method = request.method
        if view in config.AUTH_MIDDLEWARE_VIEWS and req_method in config.AUTH_MIDDLEWARE_VIEWS[view]:
            token = request.headers.get('app-token')
            if token is None:
                return JsonResponse({'message': 'you must send token', 'status': False})
            user_uid = token_read(token)
            if user_uid is None:
                return JsonResponse({'message': 'your token is wrong', 'status': False})
            else:
                try:
                    user_uid = uuid.UUID(user_uid['uid']).hex
                    user = MyUser.objects.get(uid=user_uid)
                    request.auth_user = user
                except MyUser.DoesNotExist:
                    return JsonResponse({'message': 'user dose not exist', 'status': False}, status=404)
        pass


#
class OptionalAuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        view = view_func.__name__
        req_method = request.method
        if view in config.OPTIONAL_AUTH_VIEWS and req_method in config.OPTIONAL_AUTH_VIEWS[view]:
            token = request.headers.get('app-token')
            if token is None:
                request.auth_user = None
            else:
                user_uid = token_read(token)
                if user_uid is None:
                    request.auth_user = None
                else:
                    try:
                        user_uid = uuid.UUID(user_uid['uid']).hex
                        user = MyUser.objects.get(uid=user_uid)
                        request.auth_user = user
                    except MyUser.DoesNotExist:
                        request.auth_user = None
        pass


class AccessToUserPosts:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        try:
            if 'username' and 'uid' in view_kwargs:
                auth_user = request.auth_user
                user = userController.GetUser({"username": view_kwargs['username']})
                access = postController.AccessToPosts(auth_user, user)
                request.access = access
            else:
                request.access = None

        except Exception as e:
            FinalExcept(e)
