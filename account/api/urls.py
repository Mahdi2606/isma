from django.urls import path
from account.api import views as account_view
from django.views.decorators.csrf import csrf_exempt



urlpatterns = [
    path('user/', account_view.AuthUserDetail.as_view(), name='Auth_user_view'),
    path('user/<str:username>/', account_view.UserDetail.as_view(), name='User_view'),
    path('user/<str:username>/follow/', account_view.FollowUser.as_view(), name='User_view'),
    path('register/', account_view.RegisterApiView.as_view(), name='Register_view'),
    path('login/', account_view.LoginApiView.as_view(), name='Login_view')
]

