# django
from django.http import JsonResponse

# rest framework
from rest_framework.views import APIView
from rest_framework.response import Response

# serializers
from .serializers import AuthUserSerializer, UserSerializer

# lib
from account.api.lib.utils import reqToDict
from .lib.token_manager import token_generate
# controllers
from .controllers import userController, followController

# exception
from api.excepts import ShowApiError

# Register view
class RegisterApiView(APIView):

    def post(self, request):
        post = reqToDict(request.POST)

        try:
            user = userController.RegisterController(post)
            token = token_generate({'uid': str(user.uid)})

            return Response(
                {
                    'token': token,
                    'message': 'user created',
                    'status': True
                }
            )

        except Exception as e:
            return ShowApiError(e)


# Login view
class LoginApiView(APIView):

    def post(self, request):
        post = reqToDict(request.POST)
        user = userController.LoginController(post)

        if user is None:
            return Response(
                {
                    'message': 'user dose not exist',
                    'status': False
                }
            )
        token = token_generate({'uid': str(user.uid)})

        return Response({'token': token})


# Login user view
class AuthUserDetail(APIView):

    def post(self, request):
        serializer = AuthUserSerializer(request.auth_user)
        return Response(serializer.data)

    def get(self, request):
        pass


# User detail view
class UserDetail(APIView):

    def post(self, request, username):
        try:
            user = userController.GetUser({"username": username})
            serializer = UserSerializer(user, context={'auth_user': request.auth_user})
            return JsonResponse(serializer.data)
        except Exception as e:
            return ShowApiError(e)


# Follow View
class FollowUser(APIView):

    def post(self, request, username):
        auth_user = request.auth_user
        try:
            follow = followController.Add_follow(auth_user, username)
            return Response(
                {
                    "message": f'{username} followed by {auth_user.username}' if follow.status == 2 else f'{auth_user.username} send follow request for {username}',
                    "status": True
                }
            )
        except Exception as e:
            return ShowApiError(e)

    def get(self, request):
        pass

    def put(self, request, username):
        auth_user = request.auth_user
        try:
            follow = followController.Accept_follow(username, auth_user)
            return Response(
                {
                    "message": f'you are add {follow.follower} to your followers',
                    "status": True
                }
            )
        except Exception as e:
            return ShowApiError(e)

    def delete(self, request, username):
        auth_user = request.auth_user
        try:
            follow = followController.Delete_follow(username, auth_user)
            return Response(
                {
                    "message": f'you are unfollow {follow.follower}',
                    "status": True
                }
            )
        except Exception as e:
            return ShowApiError(e)


# User update view
class UserUpdate(APIView):

    def post(self, request):
        pass

    def get(self, request):
        pass

    def put(self, request):
        pass

    def delete(self, request):
        pass
