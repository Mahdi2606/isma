import uuid
from django.db import models
from django.contrib.postgres import fields
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)


class MyUserManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError('Users must have an username address')
        user = self.model(
            username=username

        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(
            username,
            password=password,
        )
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser, PermissionsMixin):
    phone = models.CharField(max_length=255, null=True, blank=True)
    uid = models.UUIDField(default=uuid.uuid4, editable=False, blank=True, unique=True)  # want to generate new unique id from this field
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True, null=True, blank=True)
    date_of_birth = models.CharField(max_length=8, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    image = fields.ArrayField(models.CharField(max_length=255), blank=True, default=list, size=10)
    short_name = models.CharField(max_length=255, null=True, blank=True)
    long_name = models.CharField(max_length=255, null=True, blank=True)
    likes = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    follower_count = models.IntegerField(default=0)
    following_count = models.IntegerField(default=0)
    comments_count = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    username = models.CharField(max_length=50, unique=True)
    caption = models.TextField(max_length=500, blank=True, null=True)
    objects = MyUserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def get_group_permissions(self, obj=None):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    def __unicode__(self):
        return self.id



class Setting(models.Model):
    is_lock_page = models.BooleanField(default=False)
    is_lock_comment = models.BooleanField(default=False)
    user = models.OneToOneField(MyUser, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.user.username


class Follow(models.Model):
    follower = models.ForeignKey(MyUser, related_name='follower', on_delete=models.CASCADE)
    following = models.ForeignKey(MyUser, related_name='following', on_delete=models.CASCADE)
    follow_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(default=1)

    class Meta:
        unique_together = ('follower', 'following')

    def __str__(self):
        return f'%s follow %s' % (self.follower, self.following)
