from rest_framework.response import Response


def ApiResponse(status=True, message=None, data=None, **kwargs):

    return Response({
        "data": {} if data is None else data,
        "message": message,
        "status": status,
        **kwargs,
    })



