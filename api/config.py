# middleware views

AUTH_MIDDLEWARE_VIEWS = {
    'AuthUserDetail': ['POST', 'GET', 'PUT', 'DELETE'],
    'FollowUser': ['POST', 'GET', 'PUT', 'DELETE'],
    'AuthPostView': ['POST', 'GET', 'PUT', 'DELETE'],
    'AuthPostUpdate': ['POST', 'GET', 'PUT', 'DELETE'],
}

OPTIONAL_AUTH_VIEWS = {
    'UserDetail': ['POST', 'PUT'],
    'UserPostsView': ['POST', 'GET'],
    'PostView': ['POST', 'GET'],
    'CommentView': ['POST', 'GET'],
    'Upload': ['POST', 'GET']
}

# SET_USER_SETTING_VIEWS = {
#     'UserDetail': ['POST', 'GET', 'PUT', 'DELETE'],
# }
