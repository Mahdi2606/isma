from django.urls import path , include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('post/', include('post.api.urls')),
    path('account/', include('account.api.urls'))
] + static(settings.API_STATIC_URL, document_root=settings.API_STATIC_ROOT)