import uuid
import time


def reqToDict(obj):
    req = dict(obj)
    data = {}
    for i in req:
        data.update({i: req[i][0]})

    return data


def get_uuid_from_str(uniqueID):
    try:
        uid = uuid.UUID(uniqueID).hex
        return uid
    except Exception as e:
        return None


def required_fields_check(req: dict, fields: list):
    valid = True
    for field in fields:
        if field in req:
            pass
        else:
            valid: False

    return valid


Current_milli_time = lambda: int(round(time.time() * 1000))
