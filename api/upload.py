from PIL import Image
from api.excepts import ErrorTypes, ErrorHandle, FinalExcept
from api.utils import Current_milli_time
from django.conf import settings
import os

def ImageUpload(files, user):
    try:
        if len(files) > 5:
            ErrorHandle(ErrorTypes.MaxUploadFile(), False)

        content_type = ['image/jpeg', 'image/png']
        if CheckFileContent(files, content_type) is not True:
            ErrorHandle(ErrorTypes.FileTypeError(content_type=content_type), False)
        medias = []
        media_thumbnail = []
        original_size = 720, 480
        thumbnail_size = 320, 240
        for file in files:
            img = Image.open(file)
            name = file.content_type.split('/')
            filename = f'api/static/upload/images/{name[0]}-{Current_milli_time()}-{user.username}'
            originalName = f'{filename}.{name[1]}'
            thumbnail = f'{filename}-thumbnail.{name[1]}'
            img.thumbnail(original_size)
            img.save(originalName)
            img.thumbnail(thumbnail_size)
            img.save(thumbnail)
            media_thumbnail.append(thumbnail)
            medias.append(originalName)
        return {
            "medias": medias,
            "thumbnail": media_thumbnail
        }

    except Exception as e:
        FinalExcept(e)


def CheckFileContent(files, content_type):
    result = True
    for file in files:
        if file.content_type in content_type:
            pass
        else:
            result = False
    return result
