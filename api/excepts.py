import os
import sys

from django.conf import settings
from rest_framework.response import Response


class ErrorTypes:

    def init(self, **kwargs):
        for key in kwargs.keys():
            setattr(self, key, kwargs[key])

    UserExist = type('UserExist', (), {'__init__': init, 'error': lambda self: f'user already exist'})
    FieldValidation = type('FieldValidation', (), {'__init__': init, 'error': lambda self: f'files not valid'})
    UserNotExist = type('UserNotExist', (), {'__init__': init, 'error': lambda self: f'user dose not exist'})
    FollowExist = type('FollowExist', (), {'__init__': init, 'error': lambda self: f'follow already exist'})
    FollowNotExist = type('FollowNotExist', (), {'__init__': init, 'error': lambda self: f'follow dose not exist'})
    FollowAccept = type('FollowAccept', (), {'__init__': init, 'error': lambda self: f'follow already accepted'})
    PasswordNotMatch = type('PasswordNotMatch', (), {'__init__': init, 'error': lambda self: f'passwords not match'})
    OtherError = type('OtherError', (), {'__init__': init, 'error': lambda self: f'something is wrong'})
    MediaOrBody = type('MediaOrBody', (), {'__init__': init, 'error': lambda self: f'media or body must be send'})
    PostNotExist = type('PostNotExist', (), {'__init__': init, 'error': lambda self: f'post dose not exist'})
    UidError = type('UidError', (), {'__init__': init, 'error': lambda self: f'id is not valid'})
    FileTypeError = type('FileTypeError', (), {'__init__': init, "error": lambda self: f'file types must be {self.content_type or []}'})
    MaxUploadFile = type('MaxUploadFile', (), {'__init__': init, 'error': lambda self: f'Uploaded files cannot be more than 5'})



class GlobalExceptions(Exception):
    def __init__(self, arguments: dict, *args, **kwargs):
        self.type = arguments['type']
        self.error = arguments['message']
        self.status = arguments['status']
        self.exception = arguments['exception']
        self.args = args
        self.kwargs = kwargs


class CustomException(GlobalExceptions):
    pass


class ErrorHandle:
    def __init__(self, ErrorType, Status=False, exception=None, **kwargs):
        if settings.DEBUG is True:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

        raise CustomException(
            {
                "exception": exception,
                "type": ErrorType,
                "message": ErrorType.error(),
                "status": Status,
                **kwargs
            }
        )


def ShowApiError(error):
    if hasattr(error, 'type'):
        return Response({
            "message": error.error,
            "status": error.status
        })
    else:
        return Response({
            "message": 'something is wrong',
            "status": False
        })


def FinalExcept(error):
    if hasattr(error, 'type'):
        ErrorHandle(error.type, error.status)
    else:
        ErrorHandle(ErrorTypes.OtherError(), False, error)
