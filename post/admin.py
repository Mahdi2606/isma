from django.contrib import admin
from django.template.loader import render_to_string
from django.utils.html import format_html

from .models import Post, Comment


# Register your models here.


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    # pass
    list_display = ('title', 'user', 'pin', 'like', 'view', 'comment_count', 'uid', 'thumbnail_img')
    # actions = ['make_important','make_not_important']
    readonly_fields = ['uid']

    fieldsets = (
        (None, {
            'fields': (('title', 'pin',), 'user', ('body',))
        }),
        ('Points', {
            'fields': (('like', 'view', 'comment_count'),)
        }),
        ('References', {
            'fields': ('hash_tags', 'mentions', 'tags', ('medias', 'thumbnail'))
        }),
    )

    def thumbnail_img(self, obj):
        temp = render_to_string('mediaShow.html', {'thumbnails': obj.thumbnail})
        return format_html(temp)


@admin.register(Comment)
class PostCommentAdmin(admin.ModelAdmin):
    list_display = ('post', 'like', 'dislike', 'sender')
