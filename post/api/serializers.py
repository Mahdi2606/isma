from rest_framework import serializers
from post.models import Post, Comment


class AuthPostSerializer(serializers.ModelSerializer):
    comments = serializers.SerializerMethodField('get_post_comments')
    user = serializers.SerializerMethodField('get_user_from_posts')
    id = serializers.SerializerMethodField('get_id')

    class Meta:
        model = Post
        fields = ['id', 'user', 'title', 'body', 'view', 'like', 'comment_count', 'pin', 'comments', 'mentions',
                  'hash_tags', 'medias', 'thumbnail']

    def get_user_from_posts(self, posts):
        user = posts.user.username
        return user

    def get_post_comments(self, posts):
        serializer = CommentSerializer(posts.comments.all(), many=True)
        return serializer.data

    def get_id(self, posts):
        return posts.uid


class PostSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField('get_user_from_posts')
    id = serializers.SerializerMethodField('get_id')

    class Meta:
        model = Post
        fields = ['id', 'user', 'title', 'body', 'view', 'like', 'comment_count', 'pin', 'mentions', 'hash_tags',
                  'medias', 'thumbnail']

    def get_id(self, posts):
        return posts.uid

    def get_user_from_posts(self, posts):
        user = posts.user.username
        return user


class UserPostsSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField('get_user_from_posts')
    id = serializers.SerializerMethodField('get_id')

    class Meta:
        model = Post
        fields = ['id', 'user', 'view', 'like', 'comment_count', 'pin', 'medias', 'thumbnail']

    def get_id(self, posts):
        return posts.uid

    def get_user_from_posts(self, posts):
        user = posts.user.username
        return user


class CommentSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField('get_commenter_from_comment')

    class Meta:
        model = Comment
        fields = ['body', 'like', 'dislike', 'sender']

    def get_commenter_from_comment(self, commenets):
        sender = commenets.sender.username
        return sender
