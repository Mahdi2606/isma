import re


def get_hash_tags_mentions_from_body(body):
    hash_tags = re.findall(r'(?<=#)\w*', body)
    mentions = re.findall(r'(?<=@)\w*', body)
    result = {
        "hash_tags": hash_tags,
        "mentions": mentions
    }
    return result
