from api.excepts import ErrorHandle, ErrorTypes, FinalExcept
from api import utils as api_utils
from .postController import GetPost
from ..lib.utils import get_hash_tags_mentions_from_body
from post.models import Comment
# controller def

def GetComments(user, postID):
    try:
        uid = api_utils.get_uuid_from_str(postID)
        if uid is None:
            ErrorHandle(ErrorTypes.UidError(), False)

        post = GetPost(user, uid)
        comments = post.comments.all()
        return comments

    except Exception as e:
        FinalExcept(e)


def AddComment(req, user, post):
    try:
        fields = ['body']
        req_validation = api_utils.required_fields_check(req, fields)
        if req_validation is not True:
            ErrorHandle(ErrorTypes.FieldValidation(), False)

        result = get_hash_tags_mentions_from_body(req['body'])
        reply = result['mentions']

        comment = Comment(
            post=post,
            sender=user,
            body=req['body'],
            reply=reply if reply is not None else [],
        )
        comment.save()
        return comment

    except Exception as e:
        FinalExcept(e)
