from post.models import Post

# exception
from api.excepts import ErrorHandle, ErrorTypes, FinalExcept

# utils
from ..lib.utils import get_hash_tags_mentions_from_body
from api import utils as api_utils

from account.models import MyUser
from account.api.controllers import followController


def CreatePost(req, medias, user):
    try:
        if medias['medias'] is None and 'body' not in req:
            ErrorHandle(ErrorTypes.MediaOrBody(), False)

        hash_tags = None
        mentions = None

        if 'body' in req:
            result = get_hash_tags_mentions_from_body(req['body'])
            hash_tags = result['hash_tags']
            mentions = result['mentions']

        post = Post(
            user=user,
            title=req['title'] if 'title' in req else None,
            body=req['body'] or None,
            pin=req['pin'] if 'pin' in req else 0,
            medias=medias['medias'] if medias['medias'] is not None else [],
            thumbnail=medias['thumbnail'] if medias['thumbnail'] is not None else [],
            mentions=mentions if mentions is not None else [],
            hash_tags=hash_tags if hash_tags is not None else [],
        )

        post.save()
        return post

    except Exception as e:
        FinalExcept(e)


def GetAllPosts(user):
    try:
        posts = Post.objects.filter(user=user)
        return posts
    except Exception as e:
        FinalExcept(e)


def GetPost(user, uid):
    try:
        post_uid = api_utils.get_uuid_from_str(uid)
        if post_uid is None:
            ErrorHandle(ErrorTypes.UidError(), False)
        try:
            posts = Post.objects.get(user=user, uid=post_uid)
            return posts
        except Post.DoesNotExist:
            ErrorHandle(ErrorTypes.PostNotExist(), False)
    except Exception as e:
        FinalExcept(e)


def UpdatePost(req, medias, user, postID):
    try:
        print(req, medias, user, postID)
        if medias['medias'] is None and 'body' not in req:
            ErrorHandle(ErrorTypes.MediaOrBody(), False)

        uid = api_utils.get_uuid_from_str(postID)
        if uid is None:
            ErrorHandle(ErrorTypes.UidError(), False)
        post = GetPost(user, uid)


        hash_tags = None
        mentions = None

        if 'body' in req:
            result = get_hash_tags_mentions_from_body(req['body'])
            hash_tags = result['hash_tags']
            mentions = result['mentions']

        print(medias)
        post.body = req['body'] or post.body,
        post.medias += medias['medias'] if medias['medias'] is not None else post.medias + []
        post.thumbnail += medias['thumbnail'] if medias['thumbnail'] is not None else post.medias + []
        post.hash_tags = hash_tags
        post.mentions = mentions
        post.pin = req['pin'] if 'pin' in req else post.pin
        post.title = req['title'] if 'title' in req else post.title
        post.save()
        return post

    except Post.DoesNotExist:
        ErrorHandle(ErrorTypes.PostNotExist(), False)

    except Exception as e:
        print(e)
        FinalExcept(e)


def DeletePost(user, postID):
    try:
        uid = api_utils.get_uuid_from_str(postID)
        if uid is None:
            ErrorHandle(ErrorTypes.UidError(), False)
        post = GetPost(user, uid)
        post.delete()

        return True

    except Post.DoesNotExist:
        ErrorHandle(ErrorTypes.PostNotExist(), False)

    except Exception as e:
        FinalExcept(e)


def AccessToPosts(follower: MyUser, following: MyUser):
    access = 2

    if following.setting.is_lock_page is True:
        if follower is not None:
            follow = followController.CheckFollow(follower, following)
            access = 0 if follow is None else follow.status
        else:
            access = 0
    return access
