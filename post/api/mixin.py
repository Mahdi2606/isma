from api.upload import ImageUpload
from .controllers import postController, commentController
from account.api.controllers import userController, followController
from api.excepts import ShowApiError, ErrorHandle, ErrorTypes, FinalExcept
from rest_framework.response import Response
import os


class mixin:
    def __init__(self, *args, **kwargs):
        print( args, kwargs)

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class AccessToPostMixin(mixin):
    # auth_user = None
    # user = None
    # username = None
    # access = None

    def dispatch(self, request, *args, **kwargs):
        try:

            self.auth_user = request.auth_user
            for key in kwargs.keys():
                setattr(self, key, kwargs[key])

            self.user = userController.GetUser({"username": self.username})
            self.access = postController.AccessToPosts(self.auth_user, self.user)

            return super().dispatch(request, *args, **kwargs)
        except Exception as e:
            return ShowApiError(e)


# class FileUploadMixin(mixin):
#
#     def dispatch(self, request, *args, **kwargs):
#         try:
#
#             self.auth_user = request.auth_user
#             for key in kwargs.keys():
#                 setattr(self, key, kwargs[key])
#             medias = None
#             files = dict(request.FILES)
#
#             if len(files['upload']) > 5:
#                 ErrorHandle(ErrorTypes.MaxUploadFile, False)
#
#             print(len(files['upload']))
#             if len(files) != 0:
#                 medias = ImageUpload(files['upload'], self.auth_user)
#
#
#
#             setattr(self, 'medias', medias)
#             return super().dispatch(request, *args, **kwargs)
#         except Exception as e:
#
#             return ShowApiError(e)
