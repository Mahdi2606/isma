import os
import sys
# controller
from .controllers import postController, commentController
from account.api.controllers import userController, followController
# rest framework
from rest_framework.views import APIView
from rest_framework.response import Response

# serializer
from .mixin import AccessToPostMixin
from .serializers import AuthPostSerializer, PostSerializer, UserPostsSerializer, CommentSerializer

# utils
from api import utils

# exception
from api.excepts import ShowApiError

# api packages
from api.response import ApiResponse
from api.upload import ImageUpload

# packages

# Create your views here.


class AuthPostsView(APIView):

    def get(self, request):
        try:
            posts = postController.GetAllPosts(request.auth_user)
            serializer = AuthPostSerializer(posts, many=True)

            return Response({"data": serializer.data})
        except Exception as e:
            return ShowApiError(e)

    def post(self, request):

        try:
            medias = None
            req = utils.reqToDict(request.data)
            auth_user = request.auth_user
            files = dict(request.FILES)
            if len(files) != 0:
                medias = ImageUpload(files['upload'], auth_user)
            post = postController.CreatePost(req, medias, auth_user)
            serializer = PostSerializer(post)
            return Response(serializer.data)
        except Exception as e:
            print(e)
            return ShowApiError(e)


class AuthPostView(APIView):

    def get(self, request, uid):
        try:
            auth_user = request.auth_user
            post = postController.GetPost(auth_user, uid)
            serializer = AuthPostSerializer(post)
            return ApiResponse(True, data=serializer.data)
        except Exception as e:
            return ShowApiError(e)

    def put(self, request, uid):

        try:
            medias = {
                'medias' : None,
                'thumbnail' : None
            }
            req = utils.reqToDict(request.data)
            auth_user = request.auth_user
            files = dict(request.FILES)
            if len(files) != 0:
                medias = ImageUpload(files['upload'], auth_user)
            post = postController.UpdatePost(req, medias, auth_user, uid)
            serializer = PostSerializer(post)
            return Response(serializer.data)
        except Exception as e:

            return ShowApiError(e)

    def delete(self, request, uid):
        try:
            auth_user = request.auth_user
            delete = postController.DeletePost(auth_user, uid)
            if delete is not True:
                return ApiResponse(False, 'post not deleted')

            return ApiResponse(True, 'post was deleted')


        except Exception as e:
            return ShowApiError(e)


class UserPostsView(AccessToPostMixin, APIView):

    def get(self, request, username):
        try:
            if self.access in [0, 1]:
                return ApiResponse(False, 'This page is private', follow=self.access)
            posts = postController.GetAllPosts(self.user)
            serializer = UserPostsSerializer(posts, many=True)
            return ApiResponse(True, data=serializer.data)
        except Exception as e:
            return ShowApiError(e)


class PostView(AccessToPostMixin, APIView):

    def get(self, request, username, uid):
        try:
            if self.access in [0, 1]:
                return ApiResponse(False, 'This page is private', follow=self.access)
            post = postController.GetPost(self.user, uid)
            serializer = PostSerializer(post)
            return ApiResponse(True, data=serializer.data)
        except Exception as e:
            return ShowApiError(e)


class CommentView(AccessToPostMixin, APIView):

    def get(self, request, username, uid):
        try:
            if self.access in [0, 1]:
                return ApiResponse(False, 'This page is private', follow=self.access)

            comments = commentController.GetComments(self.user, uid)
            serializer = CommentSerializer(comments, many=True)
            return ApiResponse(data=serializer.data)
        except Exception as e:
            return ShowApiError(e)

    def post(self, request, username, uid):
        try:
            if self.access in [0, 1]:
                return ApiResponse(False, 'This page is private', follow=self.access)

            post = postController.GetPost(self.user, uid)
            req = utils.reqToDict(request.data)
            comment = commentController.AddComment(req, self.auth_user, post)
            serializer = CommentSerializer(comment)
            return ApiResponse(True, 'Comment was sent successfully', data=serializer.data)
        except Exception as e:
            return ShowApiError(e)


class Upload(AccessToPostMixin, APIView):
    def post(self, request, username):
        try:
            return Response('upload')

        except Exception as e:
            print(e)
            return ShowApiError(e)


