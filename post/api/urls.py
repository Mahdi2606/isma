from django.urls import path
from post.api import views
urlpatterns = [
    path('auth/', views.AuthPostsView.as_view(), name='AuthPostView'),
    path('auth/<str:uid>', views.AuthPostView.as_view(), name='AuthPostView'),
    path('user/<str:username>/', views.UserPostsView.as_view(), name='UserPostView'),
    path('user/<str:username>/<str:uid>/', views.PostView.as_view(), name='PostView'),
    path('user/<str:username>/<str:uid>/comment', views.CommentView.as_view(), name='CommentView'),
    path('upload/<str:username>', views.Upload.as_view(), name='upload')
]