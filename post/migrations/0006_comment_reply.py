# Generated by Django 3.0.5 on 2020-05-14 19:43

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0005_remove_comment_reply'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='reply',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=50), blank=True, default=list, size=10),
        ),
    ]
