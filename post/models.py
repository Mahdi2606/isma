from django.db import models
from django.contrib.postgres import fields
from account.models import MyUser
import uuid
from django.utils.html import format_html
from django.template.loader import render_to_string


# Create your models here.

class Post(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, blank=True, unique=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    like = models.IntegerField(default=0)
    view = models.IntegerField(default=0)
    comment_count = models.IntegerField(default=0)
    hash_tags = fields.ArrayField(models.CharField(max_length=50), default=list, blank=True, size=10)
    mentions = fields.ArrayField(models.CharField(max_length=50), default=list, blank=True, size=10)
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='posts')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    public = models.BooleanField(default=True),
    pin = models.BooleanField(default=False)
    tags = fields.ArrayField(models.CharField(max_length=50), default=list, blank=True, size=10)
    medias = fields.ArrayField(models.CharField(max_length=255), default=list, blank=True, size=10)
    thumbnail = fields.ArrayField(models.CharField(max_length=255), default=list, blank=True, size=10)


    def __str__(self):
        return f'{self.user.username}-{self.title}-{self.created_at}'


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    sender = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name='sender')
    body = models.TextField()
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    reply = fields.ArrayField(models.CharField(max_length=50), default=list, blank=True, size=10)
    commented_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.post.id}-{self.id}-{self.commented_at}'
