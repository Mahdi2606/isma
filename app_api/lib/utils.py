import re

def form_data_to_query_dict(body, boundary):
    newBody = body.split(boundary)
    # print(body)
    # print(newBody)
    # newBody.remove('')
    stripBody = ''

    for n in newBody:
        n = n.replace('\n', '')
        n = n.replace('\r', '')
        stripBody += n

    clearBody = stripBody[0:len(stripBody) - 3]

    clearBody += 'Content-Disposition:'

    keys = re.findall('(?<=Content-Disposition: form-data; name=")\w+', clearBody)
    values = []
    for key in keys:
        values.append(re.findall(f'(?<={key}")\S*(?=Content-Disposition)', clearBody)[0])

    queryDict = {}

    i = 0
    while i < len(keys):
        queryDict.update({keys[i]: values[i]})
        i += 1

    return queryDict
